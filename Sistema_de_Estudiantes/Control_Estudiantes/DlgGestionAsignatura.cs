﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Control_Estudiantes
{
    public partial class DlgGestionAsignatura : Form
    {
        private DataTable tblAsignatura;
        private DataSet dsAsignatura;
        private BindingSource bsAsignatura;
        private DataRow drAsignatura;

        public DlgGestionAsignatura()
        {
            InitializeComponent();
            bsAsignatura = new BindingSource();
        }//------------------------

        public DataRow DrAsignatura
        {
            set
            {
                drAsignatura = value;
                txtAsignatura.Text = drAsignatura["Nombre"].ToString();
            }
        }//------------------------

        public DataTable TblAsignatura
        {
            get
            {
                return tblAsignatura;
            }

            set
            {
                tblAsignatura = value;
            }
        }

        public DataSet DsAsignatura
        {
            get
            {
                return dsAsignatura;
            }

            set
            {
                dsAsignatura = value;
            }
        }
        
        private void DlgGestionAsignatura_Load(object sender, EventArgs e)
        {
            bsAsignatura.DataSource = DsAsignatura;
            bsAsignatura.DataMember = DsAsignatura.Tables["Asignatura"].TableName;
        }//-----------------------

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            string nombre = null;

            nombre = txtAsignatura.Text;

            if (drAsignatura != null)
            {
                DataRow drNew = TblAsignatura.NewRow();

                int index = TblAsignatura.Rows.IndexOf(drAsignatura);
                drNew["Id"] = drAsignatura["Id"];
                drNew["Nombre"] = nombre;
                
                TblAsignatura.Rows.RemoveAt(index);
                TblAsignatura.Rows.InsertAt(drNew, index);
                TblAsignatura.Rows[index].AcceptChanges();
                TblAsignatura.Rows[index].SetModified();

            }
            else
            {
                TblAsignatura.Rows.Add(TblAsignatura.Rows.Count + 1, nombre);
            }
            Dispose();
        }//---------------------

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }//--------------------
    }//Final de la clase
}//Final de la solucion
