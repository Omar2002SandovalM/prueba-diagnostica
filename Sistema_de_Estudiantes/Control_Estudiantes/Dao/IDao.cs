﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos
{
    interface IDao <T>
    {
        void Save(T t);
        bool Update(T t);
        bool Delete(T t);
        List<T> FindAll();

    }//Final de la Interfaz
}//Final de la solucion
