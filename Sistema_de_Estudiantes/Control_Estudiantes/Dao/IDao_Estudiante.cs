﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos.Dao
{
    interface IDao_Estudiante : IDao<Estudiantes>
    {
        Estudiantes FindById(int id);
        Estudiantes FindByCarnet(string Carnet);
        List<Estudiantes> FindByLastName(string Apellido);
    }//final de la clase
}//final de la solucion
