﻿using CapaDatos;
using Control_Estudiantes.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control_Estudiantes.Dao
{
    interface IDao_Inscripcion : IDao<Inscripcion>
    {
        Inscripcion FindById(int id);
    }//Final
}//Final de la solucion
