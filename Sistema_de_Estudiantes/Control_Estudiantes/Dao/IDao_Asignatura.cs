﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos.Dao
{
    interface IDao_Asignatura : IDao<Asignatura>
    {
        Asignatura FindById(int id);
        Asignatura FindByNombre(string Nombre);
    }//Final de la interfaz
}//final de la solucion
