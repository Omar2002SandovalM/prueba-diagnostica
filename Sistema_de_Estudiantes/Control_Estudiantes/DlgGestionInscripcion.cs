﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Control_Estudiantes
{
    public partial class DlgGestionInscripcion : Form
    {
        private DataTable tblInscripcion;
        private DataSet dsInscripcion;
        private BindingSource bsInscripcion;
        private DataRow drInscripcion;

        public DlgGestionInscripcion()
        {
            InitializeComponent();
            bsInscripcion = new BindingSource();
        }//Final

        public DataRow DrInscripcion
        {
            set
            {
                drInscripcion = value;
                cmbAlumnos.Text = drInscripcion["Estudiante"].ToString();
                cmbClases.Text = drInscripcion["Asignatura"].ToString();
            }
        }//------------------------

        public DataTable TblInscripcion
        {
            get
            {
                return tblInscripcion;
            }

            set
            {
                tblInscripcion = value;
            }
        }

        public DataSet DsInscripcion
        {
            get
            {
                return dsInscripcion;
            }

            set
            {
                dsInscripcion = value;
            }
        }

        private void DlgGestionInscripcion_Load(object sender, EventArgs e)
        {
            bsInscripcion.DataSource = DsInscripcion;
            bsInscripcion.DataMember = DsInscripcion.Tables["Inscripcion"].TableName;
            
        }//-----------------------------

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            string Estudiante,Asigantura;

            Estudiante = cmbAlumnos.Text;
            Asigantura = cmbClases.Text;

            if (drInscripcion!= null)
            {
                DataRow drNew = TblInscripcion.NewRow();

                int index = TblInscripcion.Rows.IndexOf(drInscripcion);
                drNew["Id"] = drInscripcion["Id"];
                drNew["Grupo"] = "2M1";
                drNew["Estudiante"] = Estudiante;
                drNew["Asignatura"] = Asigantura;

                TblInscripcion.Rows.RemoveAt(index);
                TblInscripcion.Rows.InsertAt(drNew, index);
                TblInscripcion.Rows[index].AcceptChanges();
                TblInscripcion.Rows[index].SetModified();

            }
            else
            {
                TblInscripcion.Rows.Add(TblInscripcion.Rows.Count + 1, "2M1",Estudiante,Asigantura);
            }
            Dispose();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }///Final 

}//Final 
