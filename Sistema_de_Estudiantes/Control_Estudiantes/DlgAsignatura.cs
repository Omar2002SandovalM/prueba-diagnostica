﻿using CapaDatos.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Control_Estudiantes
{
    public partial class DlgAsignatura : Form
    {
        private DataSet dsAsignatura;
        private BindingSource bsAsignatura;
        private Asignatura_Modelo AsignModel;

        public DataSet DsAsignatura
        {
            get
            {
                return dsAsignatura;
            }

            set
            {
                dsAsignatura = value;
            }
        }//----------------------

        public DlgAsignatura()
        {
            InitializeComponent();
            bsAsignatura = new BindingSource();
            AsignModel = new Asignatura_Modelo();
        }//-----------------------

        private void txtFinder_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsAsignatura.Filter = string.Format("Sku like '*{0}*' or Nombre like '*{0}*' or Descripcion like '*{0}*' ", txtFinder.Text);

            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }//-------------------

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            DlgGestionAsignatura As = new DlgGestionAsignatura();
            As.TblAsignatura = DsAsignatura.Tables["Asignatura"];
            As.DsAsignatura = DsAsignatura;
            As.ShowDialog();
        }//--------------------------

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = DG_Asignaturas.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DlgGestionAsignatura As = new DlgGestionAsignatura();
            As.TblAsignatura = DsAsignatura.Tables["Asignatura"];
            As.DsAsignatura = DsAsignatura;
            As.DrAsignatura = drow;
            As.ShowDialog();
        }//-------------------------------

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = DG_Asignaturas.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "Realmente desea eliminar ese registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                DsAsignatura.Tables["Asignatura"].Rows.Remove(drow);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }//--------------------

        private void DlgAsignatura_Load(object sender, EventArgs e)
        {
            bsAsignatura.DataSource = DsAsignatura;
            bsAsignatura.DataMember = DsAsignatura.Tables["Asignatura"].TableName;
            DG_Asignaturas.DataSource = bsAsignatura;
            DG_Asignaturas.AutoGenerateColumns = true;
        }//--------------------

        private void DlgAsignatura_FormClosing(object sender, FormClosingEventArgs e)
        {
            DataTable dtAsiganaturaAdded = dsAsignatura.Tables["Asignatura"].GetChanges(DataRowState.Added);
            DataTable dtAsignaturaUpdated = dsAsignatura.Tables["Asignatura"].GetChanges(DataRowState.Modified);

            if (dtAsiganaturaAdded != null)
            {
                foreach (DataRow dr in dtAsiganaturaAdded.Rows)
                {
                    AsignModel.Save(dr);
                    dsAsignatura.Tables["Asignatura"].Rows.Find(dr["Id"]).AcceptChanges();
                }

            }

            if (dtAsignaturaUpdated != null)
            {
                foreach (DataRow dr in dtAsignaturaUpdated.Rows)
                {
                    AsignModel.update(dr);
                    dsAsignatura.Tables["Asignatura"].Rows.Find(dr["Id"]).AcceptChanges();
                }
            }
        }//----------------------

    }//Final de la Clase
}//Final de la Solucion
