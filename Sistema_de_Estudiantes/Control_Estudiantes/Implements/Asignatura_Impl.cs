﻿using CapaDatos.Dao;
using CapaDatos.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos.Implements
{
    class Asignatura_Impl : IDao_Asignatura
    {
        //------- Declaracio de Variables
        private BinaryWriter bwHeader;
        private BinaryWriter bwData;

        private BinaryReader brHeader;
        private BinaryReader brData;

        private FileStream fsHeader;
        private FileStream fsData;

        private const int SIZE = 104;

        private List<HeaderIndex> headerIndices;
        private Header header;
        
        //------- Constructor
        public Asignatura_Impl() { }

        //------- Metodos de Abrir y cerrar Flujo
        private void Open()
        {
            try
            {
                headerIndices = new List<HeaderIndex>()
                {
                    new HeaderIndex()
                    {
                        N = 0,
                        K = 0,
                        NameHeaderIndex = "Id_Asignatura"
                    },

                    new HeaderIndex()
                    {
                        N = 0,
                        K = 0,
                        NameHeaderIndex = "Nombre_Asignatura"
                    }
                };

                header = new Header()
                {
                    N = 0,
                    Name = "hAsignaturas",
                    HeaderIndices = headerIndices
                };
                
                fsHeader = new FileStream("hAsignaturas.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fsData = new FileStream("dAsignaturas.data", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                bwHeader = new BinaryWriter(fsHeader);
                brHeader = new BinaryReader(fsHeader);

                bwData = new BinaryWriter(fsData);
                brData = new BinaryReader(fsData);
                if (fsHeader.Length == 0)
                {
                    bwHeader.Write(0);//valores N
                    bwHeader.Write(0);//valores k
                }

            }
            catch (IOException ex)
            {
                throw ex;
            }

        }//--Final

        private void Close()
        {
            try
            {
                if (bwData != null)
                {
                    bwData.Close();
                }
                if (bwHeader != null)
                {
                    bwHeader.Close();
                }
                if (brData != null)
                {
                    brData.Close();
                }
                if (brHeader != null)
                {
                    brHeader.Close();
                }
                if (fsData != null)
                {
                    fsData.Close();
                }
                if (fsHeader != null)
                {
                    fsHeader.Close();
                }
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }

        }//--Final 

        //=================================================================|| METODOS DE LAS INTERFACES ||==================================================||
        public Asignatura FindById(int id)
        {
            Asignatura clase = null;
            try
            {
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                int index = Finder.BinarySearchById(brHeader, id, 0, n - 1);

                if (index < 0)
                {
                    return clase;
                }

                long dpos = index * SIZE;
                brData.BaseStream.Seek(dpos, SeekOrigin.Begin);
                clase = new Asignatura()
                {
                    Id = brData.ReadInt32(),
                    Nombre = brData.ReadString()
                };

                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }
            return clase;
        }// FINAL DE LA BUSQUEDA POR ID DE ASGINATURA

        public Asignatura FindByNombre(string Nombre)
        {
            Asignatura clase = null;
            try
            {
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();
                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);

                    int index = brHeader.ReadInt32();

                    long dpos = (index - 1) * SIZE;
                    brData.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    int dId = brData.ReadInt32();
                    string dNombre = brData.ReadString();

                    if (!Nombre.Equals(dNombre, StringComparison.CurrentCultureIgnoreCase))
                    {
                        continue;
                    }
                    clase = new Asignatura()
                    {
                        Id = dId,
                        Nombre = brData.ReadString()
                    };
                    break;
                }
                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }
            return clase;

        }//FINAL DEL METODO DE BUSQUEDA POR NOMBRE DE ASIGNATURA

        public void Save(Asignatura t)
        {
            try
            {
                Open();

                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                long pos = k * SIZE;
                bwData.BaseStream.Seek(pos, SeekOrigin.Begin);

                bwData.Write(++k);
                bwData.Write(t.Nombre);

                bwHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                bwHeader.Write(++n);
                bwHeader.Write(k);

                long hpos = 8 + (n - 1) * 4;
                bwHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                bwHeader.Write(k);

                Close();
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }

        }//FINAL DEL METODO DE GUARDAR DATOS

        public bool Update(Asignatura t)
        {
            try
            {
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int pos = Finder.BinarySearchById(brHeader, t.Id, 0, n);
                if (pos < 0)
                {
                    return false;
                }

                long hpos = 8 + 4 * (pos);
                brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int id = brHeader.ReadInt32();

                long dpos = (id - 1) * SIZE;
                brData.BaseStream.Seek(dpos, SeekOrigin.Begin);
                bwData.Write(t.Id);
                bwData.Write(t.Nombre);

                Close();
                return true;
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }

        }//FINAL DE EL METODO PARA ACTUALIZAR LOS DATOS

        public bool Delete(Asignatura t)
        {
            FileStream temporal = new FileStream("temp.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            try
            {
                BinaryReader reader = new BinaryReader(temporal);
                BinaryWriter writer = new BinaryWriter(temporal);
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                writer.BaseStream.Seek(0, SeekOrigin.Begin);
                writer.Write(n - 1);
                writer.Write(k);
                int j = 0;
                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                    int id = brHeader.ReadInt32();
                    if (id == t.Id)
                    {
                        continue;
                    }
                    long tmpPos = 8 + 4 * j++;
                    writer.BaseStream.Seek(tmpPos, SeekOrigin.Begin);
                    writer.Write(id);
                }
                Close();
                File.Delete("hAsignaturas.dat");
                writer.Close();
                reader.Close();
                temporal.Close();
                File.Move("temp.dat", "hAsignaturas.dat");

            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }

            return false;
        }//FINAL DEL METODO DE ELIMINAR

        public List<Asignatura> FindAll()
        {
            List<Asignatura> AllClases = new List<Asignatura>();
            try
            {
                Open();

                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);

                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();
                Asignatura clase = null;
                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);

                    int index = brHeader.ReadInt32();

                    long dpos = (index - 1) * SIZE;
                    brData.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    clase = new Asignatura()
                    {
                        Id = brData.ReadInt32(),
                        Nombre = brData.ReadString()
                    };

                    AllClases.Add(clase);
                }
                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }

            return AllClases;
        }
    }//Final de la clase
}//Final de la solucion
