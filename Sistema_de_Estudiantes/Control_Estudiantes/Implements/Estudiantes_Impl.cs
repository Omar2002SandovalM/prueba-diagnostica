﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos.Dao;
using CapaDatos.Utilities;

namespace CapaDatos.Implements
{
    class Estudiantes_Impl : IDao_Estudiante
    {

        private BinaryWriter bwHeader;
        private BinaryWriter bwData;

        private BinaryReader brHeader;
        private BinaryReader brData;

        private FileStream fsHeader;
        private FileStream fsData;

        private const int SIZE = 104;

        private List<HeaderIndex> headerIndices;
        private Header header;

        //------- Constructor
        public Estudiantes_Impl() { }

        //------- Metodos de Abrir y cerrar Flujo
        private void Open()
        {
            try
            {
                headerIndices = new List<HeaderIndex>()
                {
                    new HeaderIndex()
                    {
                        N = 0,
                        K = 0,
                        NameHeaderIndex = "IdEstudiante"
                    },
                    new HeaderIndex()
                    {
                        N = 0,
                        K = 0,
                        NameHeaderIndex = "CarnetEstudiante"
                    },
                     new HeaderIndex()
                    {
                        N = 0,
                        K = 0,
                        NameHeaderIndex = "ApellidosEstudiante"
                    }
                };

                header = new Header()
                {
                    N = 0,
                    Name = "hEstudiantes",
                    HeaderIndices = headerIndices
                };

                fsHeader = new FileStream("hEstudiantes.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fsData = new FileStream("dEstudiantes.data", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                bwHeader = new BinaryWriter(fsHeader);
                brHeader = new BinaryReader(fsHeader);

                bwData = new BinaryWriter(fsData);
                brData = new BinaryReader(fsData);
                if (fsHeader.Length == 0)
                {
                    bwHeader.Write(0);//valores N
                    bwHeader.Write(0);//valores k
                }

            }
            catch (IOException ex)
            {
                throw ex;
            }

        }//--Final

        private void Close()
        {
            try
            {
                if (bwData != null)
                {
                    bwData.Close();
                }
                if (bwHeader != null)
                {
                    bwHeader.Close();
                }
                if (brData != null)
                {
                    brData.Close();
                }
                if (brHeader != null)
                {
                    brHeader.Close();
                }
                if (fsData != null)
                {
                    fsData.Close();
                }
                if (fsHeader != null)
                {
                    fsHeader.Close();
                }
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }

        }//--Final 

        //||==================================================================|| METODOS  DE LA INTERFAZ||==============================================
        public bool Delete(Estudiantes t)
        {
            FileStream temporal = new FileStream("temp.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            try
            {
                BinaryReader reader = new BinaryReader(temporal);
                BinaryWriter writer = new BinaryWriter(temporal);
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                writer.BaseStream.Seek(0, SeekOrigin.Begin);
                writer.Write(n - 1);
                writer.Write(k);
                int j = 0;
                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                    int id = brHeader.ReadInt32();
                    if (id == t.Id)
                    {
                        continue;
                    }
                    long tmpPos = 8 + 4 * j++;
                    writer.BaseStream.Seek(tmpPos, SeekOrigin.Begin);
                    writer.Write(id);
                }
                Close();
                File.Delete("hEstudiantes.dat");
                writer.Close();
                reader.Close();
                temporal.Close();
                File.Move("temp.dat", "hEstudiantes.dat");

            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }

            return false;
        }

        public List<Estudiantes> FindAll()
        {
            List<Estudiantes> AllEstudiantes = new List<Estudiantes>();
            try
            {
                Open();

                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);

                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();
                Estudiantes est = null;
                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);

                    int index = brHeader.ReadInt32();

                    long dpos = (index - 1) * SIZE;
                    brData.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    est = new Estudiantes()
                    {
                        Id = brData.ReadInt32(),
                        Nombre = brData.ReadString(),
                        Apellidos = brData.ReadString(),
                        Genero = brData.ReadString(),
                        Carnet = brData.ReadString(),
                        Grupo = brData.ReadString()
                    };

                    AllEstudiantes.Add(est);
                }
                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }

            return AllEstudiantes;
        }

        public Estudiantes FindByCarnet(string Carnet)
        {
            Estudiantes estudiantes = null;
            try
            {
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();
                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);

                    int index = brHeader.ReadInt32();

                    long dpos = (index - 1) * SIZE;
                    brData.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    int dId = brData.ReadInt32();
                    string dCarnet = brData.ReadString();

                    if (!Carnet.Equals(dCarnet, StringComparison.CurrentCultureIgnoreCase))
                    {
                        continue;
                    }
                    estudiantes = new Estudiantes()
                    {
                        Id = dId,
                        Nombre = brData.ReadString(),
                        Apellidos = brData.ReadString(),
                        Genero = brData.ReadString(),
                        Carnet = brData.ReadString(),
                        Grupo = brData.ReadString()
                    };
                    break;
                }
                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }
            return estudiantes;
        }

        public Estudiantes FindById(int id)
        {
            Estudiantes estudiantes = null;
            try
            {
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                int index = Finder.BinarySearchById(brHeader, id, 0, n - 1);

                if (index < 0)
                {
                    return estudiantes;
                }

                long dpos = index * SIZE;
                brData.BaseStream.Seek(dpos, SeekOrigin.Begin);
                estudiantes = new Estudiantes()
                {
                    Id = brData.ReadInt32(),
                    Nombre = brData.ReadString(),
                    Apellidos = brData.ReadString(),
                    Genero = brData.ReadString(),
                    Carnet = brData.ReadString(),
                    Grupo = brData.ReadString()
                };

                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }
            return estudiantes;
        }

        public List<Estudiantes> FindByLastName(string Apellido)
        {
            List<Estudiantes> estudiantesList= new List<Estudiantes>();
            try
            {
                Open();

                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);

                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();
                Estudiantes estudiantes = null;
                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);

                    int index = brHeader.ReadInt32();

                    long dpos = (index - 1) * SIZE;
                    brData.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    int dId = brData.ReadInt32();
                    string dApellido= brData.ReadString();

                    if (!Apellido.Equals(dApellido, StringComparison.CurrentCultureIgnoreCase))
                    {
                        continue;
                    }
                    estudiantes = new Estudiantes()
                    {
                        Id = dId,
                        Nombre = brData.ReadString(),
                        Apellidos = brData.ReadString(),
                        Genero = brData.ReadString(),
                        Carnet = brData.ReadString(),
                        Grupo = brData.ReadString()
                    };
                    break;
                }

                estudiantesList.Add(estudiantes);

                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }

            return estudiantesList;
        }

        public void Save(Estudiantes t)
        {
            try
            {
                Open();

                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                long pos = k * SIZE;
                bwData.BaseStream.Seek(pos, SeekOrigin.Begin);

                bwData.Write(++k);
                bwData.Write(t.Nombre);
                bwData.Write(t.Apellidos);
                bwData.Write(t.Genero);
                bwData.Write(t.Carnet);
                bwData.Write(t.Grupo);

                bwHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                bwHeader.Write(++n);
                bwHeader.Write(k);

                long hpos = 8 + (n - 1) * 4;
                bwHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                bwHeader.Write(k);

                Close();
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }

        }

        public bool Update(Estudiantes t)
        {
            try
            {
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int pos = Finder.BinarySearchById(brHeader, t.Id, 0, n);
                if (pos < 0)
                {
                    return false;
                }

                long hpos = 8 + 4 * (pos);
                brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int id = brHeader.ReadInt32();

                long dpos = (id - 1) * SIZE;
                brData.BaseStream.Seek(dpos, SeekOrigin.Begin);

                bwData.Write(t.Id);
                bwData.Write(t.Nombre);
                bwData.Write(t.Apellidos);
                bwData.Write(t.Genero);
                bwData.Write(t.Carnet);
                bwData.Write(t.Grupo);

                Close();
                return true;
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }
        }
    }//Final de la clase
}//Final de la solucion
