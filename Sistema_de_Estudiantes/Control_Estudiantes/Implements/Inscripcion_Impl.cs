﻿using CapaDatos.Utilities;
using Control_Estudiantes.Dao;
using Control_Estudiantes.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control_Estudiantes.Implements
{
    class Inscripcion_Impl : IDao_Inscripcion
    {

        //------- Declaracio de Variables
        private BinaryWriter bwHeader;
        private BinaryWriter bwData;

        private BinaryReader brHeader;
        private BinaryReader brData;

        private FileStream fsHeader;
        private FileStream fsData;

        private const int SIZE = 200;

        private List<HeaderIndex> headerIndices;
        private Header header;

        //------- Metodos de Abrir y cerrar Flujo
        private void Open()
        {
            try
            {
                headerIndices = new List<HeaderIndex>()
                {
                    new HeaderIndex()
                    {
                        N = 0,
                        K = 0,
                        NameHeaderIndex = "Id_Inscripcion"
                    },
                };

                header = new Header()
                {
                    N = 0,
                    Name = "hInscripcion",
                    HeaderIndices = headerIndices
                };

                fsHeader = new FileStream("hInscripcion.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fsData = new FileStream("dInscripcion.data", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                bwHeader = new BinaryWriter(fsHeader);
                brHeader = new BinaryReader(fsHeader);

                bwData = new BinaryWriter(fsData);
                brData = new BinaryReader(fsData);
                if (fsHeader.Length == 0)
                {
                    bwHeader.Write(0);//valores N
                    bwHeader.Write(0);//valores k
                }

            }
            catch (IOException ex)
            {
                throw ex;
            }

        }//--Final

        private void Close()
        {
            try
            {
                if (bwData != null)
                {
                    bwData.Close();
                }
                if (bwHeader != null)
                {
                    bwHeader.Close();
                }
                if (brData != null)
                {
                    brData.Close();
                }
                if (brHeader != null)
                {
                    brHeader.Close();
                }
                if (fsData != null)
                {
                    fsData.Close();
                }
                if (fsHeader != null)
                {
                    fsHeader.Close();
                }
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }

        }//--Final 

        public bool Delete(Inscripcion t)
        {
            FileStream temporal = new FileStream("temp_Ins.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            try
            {
                BinaryReader reader = new BinaryReader(temporal);
                BinaryWriter writer = new BinaryWriter(temporal);
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                writer.BaseStream.Seek(0, SeekOrigin.Begin);
                writer.Write(n - 1);
                writer.Write(k);
                int j = 0;
                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                    int id = brHeader.ReadInt32();
                    if (id == t.Id)
                    {
                        continue;
                    }
                    long tmpPos = 8 + 4 * j++;
                    writer.BaseStream.Seek(tmpPos, SeekOrigin.Begin);
                    writer.Write(id);
                }
                Close();
                File.Delete("hInscripcion.dat");
                writer.Close();
                reader.Close();
                temporal.Close();
                File.Move("temp.dat", "hInscripcion.dat");

            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }

            return false;
        }//-----------------------------

        public List<Inscripcion> FindAll()
        {
            List<Inscripcion> AllInscripcion = new List<Inscripcion>();
            try
            {
                Open();

                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);

                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();
                Inscripcion insc = null;
                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);

                    int index = brHeader.ReadInt32();

                    long dpos = (index - 1) * SIZE;
                    brData.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    insc = new Inscripcion()
                    {
                        Id = brData.ReadInt32(),
                        Alumno = brData.ReadString(),
                        Clase = brData.ReadString()
                    };

                    AllInscripcion.Add(insc);
                }
                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }

            return AllInscripcion;
        }

        public Inscripcion FindById(int id)
        {
            Inscripcion inscripcion = null;
            try
            {
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                int index = Finder.BinarySearchById(brHeader, id, 0, n - 1);

                if (index < 0)
                {
                    return inscripcion;
                }

                long dpos = index * SIZE;
                brData.BaseStream.Seek(dpos, SeekOrigin.Begin);
                inscripcion = new Inscripcion()
                {
                    Id = brData.ReadInt32(),
                    Alumno = brData.ReadString(),
                    Clase = brData.ReadString()
                };

                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }
            return inscripcion;
        }//--------------

        public void Save(Inscripcion t)
        {
            try
            {
                Open();

                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                long pos = k * SIZE;
                bwData.BaseStream.Seek(pos, SeekOrigin.Begin);

                bwData.Write(++k);
                bwData.Write(t.Alumno);
                bwData.Write(t.Clase);

                bwHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                bwHeader.Write(++n);
                bwHeader.Write(k);

                long hpos = 8 + (n - 1) * 4;
                bwHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                bwHeader.Write(k);

                Close();
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }
        }//------------------

        public bool Update(Inscripcion t)
        {
            try
            {
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int pos = Finder.BinarySearchById(brHeader, t.Id, 0, n);
                if (pos < 0)
                {
                    return false;
                }

                long hpos = 8 + 4 * (pos);
                brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int id = brHeader.ReadInt32();

                long dpos = (id - 1) * SIZE;
                brData.BaseStream.Seek(dpos, SeekOrigin.Begin);
                bwData.Write(t.Id);
                bwData.Write(t.Alumno);
                bwData.Write(t.Clase);

                Close();
                return true;
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }
        }//----------------
    }//FInal de la clase
}//Final de la solucoin
