﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Control_Estudiantes
{
    public partial class DlgGestionEstudiante : Form
    {
        private DataTable tblEstudiante;
        private DataSet dsEstudiante;
        private BindingSource bsEstudiante;
        private DataRow drEstudiante;

        public DlgGestionEstudiante()
        {
            InitializeComponent();
            bsEstudiante = new BindingSource();
        }//----

        public DataRow DrEstudiante
        {
            set
            {
                drEstudiante = value;
                txtNombre.Text = drEstudiante["Nombre"].ToString();
                txtApellido.Text = drEstudiante["Apellido"].ToString();
                txtGenero.Text = drEstudiante["Genero"].ToString();
                txtCarnet.Text = drEstudiante["Carnet"].ToString();
                txtGrupo.Text = drEstudiante["Grupo"].ToString();
            }

        }//-------------------------------

        public DataTable TblEstudiante
        {
            get
            {
                return tblEstudiante;
            }

            set
            {
                tblEstudiante = value;
            }
        }//-----------------------------------

        public DataSet DsEstudiante
        {
            get
            {
                return dsEstudiante;
            }

            set
            {
                dsEstudiante = value;
            }
        }//----------------------------------

        private void btbAdd_Click(object sender, EventArgs e)
        {
            string nombre, apellidos, genero, carnet, grupo;

            nombre = txtNombre.Text;
            apellidos = txtApellido.Text;
            genero = txtGenero.Text;
            carnet = txtCarnet.Text;
            grupo = txtGrupo.Text;

            if (drEstudiante != null) {

                DataRow drNew = TblEstudiante.NewRow();

                int index = TblEstudiante.Rows.IndexOf(drEstudiante);
                drNew["Id"] = drEstudiante["Id"];
                drNew["Nombre"] = nombre;
                drNew["Apellido"] = apellidos;
                drNew["Genero"] = genero;
                drNew["Carnet"] = carnet;
                drNew["Grupo"] = grupo;


                TblEstudiante.Rows.RemoveAt(index);
                TblEstudiante.Rows.InsertAt(drNew, index);
                TblEstudiante.Rows[index].AcceptChanges();
                TblEstudiante.Rows[index].SetModified();

            }
            else
            {
                TblEstudiante.Rows.Add(TblEstudiante.Rows.Count + 1, nombre,apellidos, genero, carnet, grupo);
            }

            Dispose();
        }//--------------------------

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            Dispose();
        }//-------------------------

        private void DlgGestionEstudiante_Load(object sender, EventArgs e)
        {
            //bsEstudiante.DataSource = DsEstudiante;
            //bsEstudiante.DataMember = DsEstudiante.Tables["Estudiante"].TableName;
        }//-------------------------
        
    }//Final de la  clase
}//Final de la solucion
