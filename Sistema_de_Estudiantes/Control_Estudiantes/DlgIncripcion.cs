﻿using Control_Estudiantes.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Control_Estudiantes
{
    public partial class DlgIncripcion : Form
    {
        private DataSet dsInscripcion;
        private BindingSource bsInscripcion;
        private Inscripcion_Modelo InsModel;
        
        public DlgIncripcion()
        {
            InitializeComponent();
            bsInscripcion = new BindingSource();
            InsModel = new Inscripcion_Modelo();
        }//----------------------
        public DataSet DsInscripcion
        {
            get
            {
                return dsInscripcion;
            }

            set
            {
                dsInscripcion = value;
            }
        }//----------------------

        private void DlgIncripcion_Load(object sender, EventArgs e)
        {
            bsInscripcion.DataSource = DsInscripcion;
            bsInscripcion.DataMember = DsInscripcion.Tables["Inscripcion"].TableName;
            DG_Inscripcion.DataSource = bsInscripcion;
            DG_Inscripcion.AutoGenerateColumns = true;
        }//*----------------------

        private void DlgIncripcion_FormClosing(object sender, FormClosingEventArgs e)
        {
            DataTable dtInscripcionAdded = dsInscripcion.Tables["Inscripcion"].GetChanges(DataRowState.Added);
            DataTable dtInscripcionUpdated = dsInscripcion.Tables["Inscripcion"].GetChanges(DataRowState.Modified);

            if (dtInscripcionAdded != null)
            {
                foreach (DataRow dr in dtInscripcionAdded.Rows)
                {
                    InsModel.Save(dr);
                    dsInscripcion.Tables["Inscripcion"].Rows.Find(dr["Id"]).AcceptChanges();
                }

            }

            if (dtInscripcionUpdated != null)
            {
                foreach (DataRow dr in dtInscripcionUpdated.Rows)
                {
                    InsModel.update(dr);
                    dsInscripcion.Tables["Inscripcion"].Rows.Find(dr["Id"]).AcceptChanges();
                }
            }
        }//----------------------

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            DlgGestionInscripcion As = new DlgGestionInscripcion();
            As.TblInscripcion = DsInscripcion.Tables["Inscripcion"];
            As.DsInscripcion = DsInscripcion;
            As.ShowDialog();
        }//------------------------

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = DG_Inscripcion.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DlgGestionInscripcion As = new DlgGestionInscripcion();
            As.TblInscripcion = DsInscripcion.Tables["Inscripcion"];
            As.DsInscripcion = DsInscripcion;
            As.DrInscripcion= drow;
            As.ShowDialog();

        }//---------

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = DG_Inscripcion.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "Realmente desea eliminar ese registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                DsInscripcion.Tables["Inscripcion"].Rows.Remove(drow);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }//----------
    }//Final
}//Final 
