﻿namespace Control_Estudiantes
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dsAll = new System.Data.DataSet();
            this.tblEstudiante = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.tblAsignatura = new System.Data.DataTable();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.tblInscripcion = new System.Data.DataTable();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.bsAll = new System.Windows.Forms.BindingSource(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.gestionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estudianteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asignaturaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.incripcionClasesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dsAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEstudiante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAsignatura)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblInscripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsAll)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dsAll
            // 
            this.dsAll.DataSetName = "NewDataSet";
            this.dsAll.Tables.AddRange(new System.Data.DataTable[] {
            this.tblEstudiante,
            this.tblAsignatura,
            this.tblInscripcion});
            // 
            // tblEstudiante
            // 
            this.tblEstudiante.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6});
            this.tblEstudiante.TableName = "Estudiante";
            // 
            // dataColumn1
            // 
            this.dataColumn1.AllowDBNull = false;
            this.dataColumn1.ColumnName = "Id";
            this.dataColumn1.DataType = typeof(int);
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "Nombre";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "Apellido";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "Genero";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "Carnet";
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "Grupo";
            // 
            // tblAsignatura
            // 
            this.tblAsignatura.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn7,
            this.dataColumn8});
            this.tblAsignatura.TableName = "Asignatura";
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "Id";
            this.dataColumn7.DataType = typeof(int);
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "Nombre";
            // 
            // tblInscripcion
            // 
            this.tblInscripcion.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn9,
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn12});
            this.tblInscripcion.TableName = "Inscripcion";
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "Id";
            this.dataColumn9.DataType = typeof(int);
            // 
            // dataColumn10
            // 
            this.dataColumn10.ColumnName = "Grupo";
            // 
            // dataColumn11
            // 
            this.dataColumn11.ColumnName = "Estudiante";
            // 
            // dataColumn12
            // 
            this.dataColumn12.ColumnName = "Asignatura";
            // 
            // bsAll
            // 
            this.bsAll.DataSource = this.dsAll;
            this.bsAll.Position = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gestionesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1124, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // gestionesToolStripMenuItem
            // 
            this.gestionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.estudianteToolStripMenuItem,
            this.asignaturaToolStripMenuItem,
            this.incripcionClasesToolStripMenuItem});
            this.gestionesToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gestionesToolStripMenuItem.Name = "gestionesToolStripMenuItem";
            this.gestionesToolStripMenuItem.Size = new System.Drawing.Size(83, 24);
            this.gestionesToolStripMenuItem.Text = "Opciones";
            // 
            // estudianteToolStripMenuItem
            // 
            this.estudianteToolStripMenuItem.Name = "estudianteToolStripMenuItem";
            this.estudianteToolStripMenuItem.Size = new System.Drawing.Size(203, 24);
            this.estudianteToolStripMenuItem.Text = "Gestion Estudiante";
            this.estudianteToolStripMenuItem.Click += new System.EventHandler(this.estudianteToolStripMenuItem_Click);
            // 
            // asignaturaToolStripMenuItem
            // 
            this.asignaturaToolStripMenuItem.Name = "asignaturaToolStripMenuItem";
            this.asignaturaToolStripMenuItem.Size = new System.Drawing.Size(203, 24);
            this.asignaturaToolStripMenuItem.Text = "Gestion Asignatura";
            this.asignaturaToolStripMenuItem.Click += new System.EventHandler(this.asignaturaToolStripMenuItem_Click);
            // 
            // incripcionClasesToolStripMenuItem
            // 
            this.incripcionClasesToolStripMenuItem.Name = "incripcionClasesToolStripMenuItem";
            this.incripcionClasesToolStripMenuItem.Size = new System.Drawing.Size(203, 24);
            this.incripcionClasesToolStripMenuItem.Text = "Incripcion clases";
            this.incripcionClasesToolStripMenuItem.Click += new System.EventHandler(this.incripcionClasesToolStripMenuItem_Click);
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1124, 632);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmPrincipal";
            this.Text = "Sistema de Notas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dsAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblEstudiante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblAsignatura)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblInscripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsAll)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Data.DataSet dsAll;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem gestionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem estudianteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asignaturaToolStripMenuItem;
        public System.Windows.Forms.BindingSource bsAll;
        private System.Data.DataTable tblEstudiante;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataTable tblAsignatura;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataTable tblInscripcion;
        private System.Data.DataColumn dataColumn9;
        private System.Data.DataColumn dataColumn10;
        private System.Data.DataColumn dataColumn11;
        private System.Data.DataColumn dataColumn12;
        private System.Windows.Forms.ToolStripMenuItem incripcionClasesToolStripMenuItem;
    }
}

