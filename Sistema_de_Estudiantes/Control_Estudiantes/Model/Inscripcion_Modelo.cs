﻿using Control_Estudiantes.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control_Estudiantes.Model
{
    class Inscripcion_Modelo
    {
        //Variables
        private static List<Inscripcion> ListAsignaturas = new List<Inscripcion>();
        private Implements.Inscripcion_Impl imp;
        //Constructor
        public Inscripcion_Modelo()
        {
            imp = new Implements.Inscripcion_Impl();
        }

        //---METODOS
        public List<Inscripcion> GetList()
        {
            return imp.FindAll();
        }//--

        public void Populate()
        {

        }//---

        public void Save(DataRow Ins)
        {
            Inscripcion a = new Inscripcion();
            a.Id = Convert.ToInt32(Ins["Id"].ToString());
            a.Alumno = Ins["Alumno"].ToString();
            a.Clase = Ins["Clase"].ToString();

            imp.Save(a);
        }//---

        public void update(DataRow Ins)
        {
            Inscripcion a = new Inscripcion();
            a.Id = Convert.ToInt32(Ins["Id"].ToString());
            a.Alumno = Ins["Alumno"].ToString();
            a.Clase = Ins["Clase"].ToString();

            imp.Update(a);
        }//------
        public Inscripcion FindById(int id)
        {
            return imp.FindById(id);
        }//-----

    }//Final  de la clase
}
