﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos.Model
{
    class Asignatura_Modelo
    {
        //Variables
        private static List<Asignatura> ListAsignaturas = new List<Asignatura>();
        private Implements.Asignatura_Impl impAsgnaturas;
        //Constructor
        public Asignatura_Modelo()
        {
            impAsgnaturas = new Implements.Asignatura_Impl();
        }

        //---METODOS
        public List<Asignatura> GetList()
        {
            return impAsgnaturas.FindAll();
        }//--

        public void Populate()
        {
            
        }//---

        public void Save(DataRow Clase)
        {
            Asignatura a = new Asignatura();
            a.Id = Convert.ToInt32(Clase["Id"].ToString());
            a.Nombre = Clase["Nombre"].ToString();

            impAsgnaturas.Save(a);
        }//---

        public void update(DataRow Clase)
        {
            Asignatura clase = new Asignatura();
            clase.Id = Convert.ToInt32(Clase["Id"].ToString());
            clase.Nombre = Clase["Nombre"].ToString();

            impAsgnaturas.Update(clase); 
        }//------
        public Asignatura FindById(int id)
        {
            return impAsgnaturas.FindById(id);
        }//-----

    }//final de la clase
}//Final de la solucion
