﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos.Model
{
    class Estudiantes_Modelo
    {

        //Variables
        private static List<Estudiantes> ListEstudinates = new List<Estudiantes>();
        private Implements.Estudiantes_Impl impEstudiantes;

        //Constructor
        public Estudiantes_Modelo()
        {
            impEstudiantes = new Implements.Estudiantes_Impl();
        }

        //---METODOS
        public List<Estudiantes> GetList()
        {
            return impEstudiantes.FindAll();
        }//--

        public void Populate()
        {
          
        }//---

        public void Save(DataRow estudiante)
        {
            Estudiantes estudiantes = new Estudiantes();
            estudiantes.Id = Convert.ToInt32(estudiante["Id"].ToString());
            estudiantes.Nombre = estudiante["Nombre"].ToString();
            estudiantes.Apellidos = estudiante["Apellido"].ToString();
            estudiantes.Genero = estudiante["Genero"].ToString();
            estudiantes.Carnet = estudiante["Carnet"].ToString();
            estudiantes.Grupo = estudiante["Grupo"].ToString();

            impEstudiantes.Save(estudiantes);
        }//---


        public void update(DataRow estudiante)
        {
            Estudiantes estudiantes = new Estudiantes();
            estudiantes.Id = Convert.ToInt32(estudiante["Id"].ToString());
            estudiantes.Nombre = estudiante["Nombre"].ToString();
            estudiantes.Apellidos = estudiante["Apellido"].ToString();
            estudiantes.Genero = estudiante["Genero"].ToString();
            estudiantes.Carnet = estudiante["Carnet"].ToString();
            estudiantes.Grupo = estudiante["Grupo"].ToString();

            impEstudiantes.Update(estudiantes);
        }//------
        public Estudiantes FindById(int id)
        {
            return impEstudiantes.FindById(id);
        }//-----
        
    }//Final de la clase
}//Final de la solucion
