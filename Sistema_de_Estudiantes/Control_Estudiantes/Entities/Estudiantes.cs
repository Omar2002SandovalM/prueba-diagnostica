﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos
{
    public class Estudiantes
    {
        private int id_estudiante;
        private string Nombres;
        private string apellidos;
        private string genero;
        private string carnet;
        private string grupo;

        public Estudiantes(){}//---
        
        public Estudiantes(int id, string nombres, string apellidos, string genero, string carnet, string grupo)
        {
            this.id_estudiante = id;
            this.Nombres = nombres;
            this.apellidos = apellidos;
            this.genero = genero;
            this.carnet = carnet;
            this.grupo = grupo;
        }//------

        public int Id
        {
            get
            {
                return id_estudiante;
            }
            set
            {
                id_estudiante = value;
            }
        }//--

        public string Nombre
        {
            get
            {
                return Nombres;
            }
            set
            {
                Nombres = value;
            }
        }//--

        public string Apellidos
        {
            get
            {
                return apellidos;
            }
            set
            {
                apellidos = value;
            }
        }//--

        public string Genero
        {
            get
            {
                return genero;
            }
            set
            {
                genero = value;
            }
        }//--

        public string Carnet
        {
            get
            {
                return carnet;
            }
            set
            {
                carnet = value;
            }
        }//--

        public string Grupo
        {
            get
            {
                return grupo;
            }
            set
            {
                grupo = value;
            }
        }//--

    }//Final de la clase
}//Final de la solucion
