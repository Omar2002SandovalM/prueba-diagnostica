﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos
{
    class Asignatura
    {
        private int id_Asignatura;// 4
        private string nombre;//100
        //Total SIZE => 100 + 4 = 104
        
        public Asignatura() { }//--

        public Asignatura(int id, string nombre)
        {
            this.id_Asignatura = id;
            this.nombre = nombre;
        }//-----

        public int Id
        {
            get
            {
                return id_Asignatura;
            }
            set
            {
                id_Asignatura = value; 
            }
        }//--

        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                nombre = value;
            }
        }//--
        
    }//Final de la clase
}//Final de la solucion
