﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control_Estudiantes.Entities
{
    class Inscripcion
    {
        private int id;
        private string grupo;
        private string alumno;
        private string clase;
        
        public Inscripcion() { }

        public Inscripcion(int id, string alumno, string clases, string grupo)
        {
            this.id = id;
            this.alumno = alumno;
            this.clase = clases;
            this.grupo = grupo;
        }//----------

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }//-----

        public string Alumno
        {
            get
            {
                return alumno;
            }
            set
            {
                alumno = value;
            }
        }//------

        public string Clase
        {
            get
            {
                return clase;
            }
            set
            {
                clase = value;
            }
        }//-------

        public string Grupo
        {
            get
            {
                return grupo;
            }

            set
            {
                grupo = value;
            }
        }
    }//Final
}//Final
