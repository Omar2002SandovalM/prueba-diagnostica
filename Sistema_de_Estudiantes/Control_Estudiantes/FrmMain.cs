﻿using CapaDatos;
using CapaDatos.Model;
using Control_Estudiantes.Entities;
using Control_Estudiantes.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Control_Estudiantes
{
    public partial class FrmPrincipal : Form
    {

        private DataTable dtEstudiante;
        private Asignatura_Modelo AsigModel;
        private Estudiantes_Modelo EstModel;
        private Inscripcion_Modelo InsModel;

        public FrmPrincipal()
        {
            InitializeComponent();
            AsigModel = new Asignatura_Modelo();
            EstModel = new Estudiantes_Modelo();
            InsModel = new Inscripcion_Modelo();
        }//------------------------------------

        private void estudianteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DlgEstudiante fgp = new DlgEstudiante();
            fgp.DsEstudiante = dsAll;
            fgp.Show();
        }//------------------------------------

        private void asignaturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DlgAsignatura fgp = new DlgAsignatura();
            fgp.DsAsignatura = dsAll;
            fgp.Show();
        }//-------------------------------------

        private void FrmMain_Load(object sender, EventArgs e)
        {
            //---------------------------------------------------------------//
            dtEstudiante = dsAll.Tables["Estudiante"];

            List<Estudiantes> ListEstudet = EstModel.GetList();
            if (ListEstudet.Count == 0)
            {
                EstModel.Populate();
                ListEstudet = EstModel.GetList();
            }

            foreach (Estudiantes E in ListEstudet)
            {
                DataRow drEstudent = dsAll.Tables["Estudiante"].NewRow();
                drEstudent["Id"] = E.Id;
                drEstudent["Nombre"] = E.Nombre;
                drEstudent["Apellido"] = E.Apellidos;
                drEstudent["Genero"] = E.Genero;
                drEstudent["Carnet"] = E.Carnet;
                drEstudent["Grupo"] = E.Grupo;

                dsAll.Tables["Estudiante"].Rows.Add(drEstudent);
                drEstudent.AcceptChanges();
            }//--Final del For 

            //-----------------------------------------------------------------//
            List<Asignatura> ListAsignatura = AsigModel.GetList();
            if (ListAsignatura.Count == 0)
            {
                AsigModel.Populate();
                ListAsignatura = AsigModel.GetList();
            }
            foreach (Asignatura A in ListAsignatura)
            {
                DataRow drAsignatura = dsAll.Tables["Asignatura"].NewRow();
                drAsignatura["Id"] = A.Id;
                drAsignatura["Nombre"] = A.Nombre;

                dsAll.Tables["Asignatura"].Rows.Add(drAsignatura);
                drAsignatura.AcceptChanges();
            }
            //-------------------------------------------------------------------//
            List<Inscripcion> ListInsription = InsModel.GetList();
            if (ListInsription.Count == 0)
            {
                InsModel.Populate();
                ListInsription = InsModel .GetList();
            }
            foreach (Inscripcion A in ListInsription)
            {
                DataRow drInscripcion = dsAll.Tables["Inscripcion"].NewRow();
                drInscripcion["Id"] = A.Id;
                drInscripcion["Grupo"] = A.Grupo;
                drInscripcion["Estudiante"] = A.Alumno;
                drInscripcion["Asignatura"] = A.Clase;

                dsAll.Tables["Inscripcion"].Rows.Add(drInscripcion);
                drInscripcion.AcceptChanges();
            }

        }//------------------------------------------------------

        private void incripcionClasesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DlgIncripcion fgp = new DlgIncripcion();
            fgp.DsInscripcion = dsAll;
            fgp.Show();
        }//-------------------------------------------------------

    }//Final de la clase
}//Final de la Solucion
