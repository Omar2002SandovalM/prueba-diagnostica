﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaDatos.Model;

namespace Control_Estudiantes
{
    public partial class DlgEstudiante : Form
    {
        private DataSet dsEstudiante;
        private BindingSource bsEstudiante;
        private Estudiantes_Modelo EstModel;
        
        public DataSet DsEstudiante
        {
            get
            {
                return dsEstudiante;
            }

            set
            {
                dsEstudiante = value;
            }
        }

        public DlgEstudiante()
        {
            InitializeComponent();
            bsEstudiante = new BindingSource();
            EstModel = new Estudiantes_Modelo();
        }//------

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            DlgGestionEstudiante DE = new DlgGestionEstudiante();
            DE.TblEstudiante = DsEstudiante.Tables["Estudiante"];
            DE.DsEstudiante = DsEstudiante;
            DE.ShowDialog();
        }//----

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = DG_Estudiantes.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DlgGestionEstudiante DE = new DlgGestionEstudiante();
            DE.TblEstudiante = DsEstudiante.Tables["Estudiante"];
            DE.DsEstudiante = DsEstudiante;
            DE.DrEstudiante = drow;
            DE.ShowDialog();
        }//-----

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = DG_Estudiantes.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "Realmente desea eliminar ese registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                DsEstudiante.Tables["Estudiante"].Rows.Remove(drow);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }//------

        private void txtFinder_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsEstudiante.Filter = string.Format("Sku like '*{0}*' or Nombre like '*{0}*' or Descripcion like '*{0}*' ", txtFinder.Text);

            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }//----------

        private void DlgEstudiante_Load(object sender, EventArgs e)
        {
            bsEstudiante.DataSource = DsEstudiante;
            bsEstudiante.DataMember = DsEstudiante.Tables["Estudiante"].TableName;
            DG_Estudiantes.DataSource = bsEstudiante;
            DG_Estudiantes.AutoGenerateColumns = true;
        }//-----------

        private void DlgEstudiante_FormClosing(object sender, FormClosingEventArgs e)
        {
            DataTable dtProductosAdded = dsEstudiante.Tables["Estudiante"].GetChanges(DataRowState.Added);
            DataTable dtProductosUpdated = dsEstudiante.Tables["Estudiante"].GetChanges(DataRowState.Modified);

            if (dtProductosAdded != null)
            {
                foreach (DataRow dr in dtProductosAdded.Rows)
                {
                    EstModel.Save(dr);
                    dsEstudiante.Tables["Estudiante"].Rows.Find(dr["Id"]).AcceptChanges();
                }

            }

            if (dtProductosUpdated != null)
            {
                foreach (DataRow dr in dtProductosUpdated.Rows)
                {
                    EstModel.update(dr);
                    dsEstudiante.Tables["Estudiante"].Rows.Find(dr["Id"]).AcceptChanges();
                }
            }
        }//-----------

    }//Final de la clase
}//final del las solucion
