/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.Sistemas.Controller;

import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import ni.edu.uni.Sistemas.Entities.Asignatura;
import ni.edu.uni.Sistemas.Model.AsignaturaModel;

/**
 * FXML Controller class
 *
 * @author Lenovo
 */
public class DlgAsignaturaController implements Initializable {

	@FXML
	private JFXTextField txtAsignatura;
	@FXML
	private Button btnGuardar;
	@FXML
	private Button btnCancel;

	/**
	 * Initializes the controller class.
	 */
	
	private ObservableList<Asignatura> observableListEmpleados;

	private Asignatura asignatura;
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// TODO
	}	

	@FXML
	private void OnActionGuardar(ActionEvent event) {
		String Asing;
		
		Asing = txtAsignatura.getText();
		Asignatura as = null;
		try {
		    if(asignatura == null){
			as = new Asignatura(Asing);
			save(as);
		    }else{
			as = new Asignatura(asignatura.getId(), Asing);
			update(as);
		    }
		    
		    clearTexts();
		} catch (IOException ex) {
		    Logger.getLogger(DlgAsignaturaController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@FXML
	private void OnActionCancel(ActionEvent event) {
		Node source = (Node) event.getSource();
		Stage stage = (Stage) source.getScene().getWindow();        
		stage.close();  
	}
	
	private void save(Asignatura a) throws IOException{
		AsignaturaModel.getDaoImpl().save(a);
		observableListEmpleados.add(a);
	}
	
	private void update(Asignatura a) throws IOException{
		AsignaturaModel.getDaoImpl().update(a);
		int index = observableListEmpleados.indexOf(asignatura);
		observableListEmpleados.set(index, a);
	}
	
	public void setObservableListEmpleados(ObservableList<Asignatura> observableListEmpleados) {
		this.observableListEmpleados = observableListEmpleados;
	}

	public void setEmpleado(Asignatura asignatura) {
	    this.asignatura = asignatura;
	    txtAsignatura.setText(asignatura.getNombre());
	    btnGuardar.setText("Actualizar");
	}

	private void clearTexts() {
		txtAsignatura.setText("");
	}
}


































































