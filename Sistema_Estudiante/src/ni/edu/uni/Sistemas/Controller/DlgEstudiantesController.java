/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.Sistemas.Controller;

import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import ni.edu.uni.Sistemas.Entities.Estudiante;
import ni.edu.uni.Sistemas.Model.EstudianteModel;

/**
 * FXML Controller class
 *
 * @author Lenovo
 */
public class DlgEstudiantesController implements Initializable {

	@FXML
	private JFXTextField txtNombre;
	@FXML
	private JFXTextField txtApellido;
	@FXML
	private JFXTextField txtGrupo;
	@FXML
	private JFXTextField txtCarnet;
	@FXML
	private Button btnGuardar;
	@FXML
	private Button btnCancelar;

	/**
	 * Initializes the controller class.
	 * @param url
	 * @param rb
	 */
	private ObservableList<Estudiante> observableListEmpleados;

	private Estudiante estudiante;
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// TODO
	}//Final	

	@FXML
	private void OnActionGuardar(ActionEvent event) {
		String nombre, apellido, grupo, carnet;
		
		nombre = txtNombre.getText();
		apellido = txtApellido.getText();
		grupo = txtGrupo.getText();
		carnet = txtCarnet.getText();

		Estudiante est = null;
		try {
		    if(estudiante == null){
			est = new Estudiante(nombre,apellido,grupo,carnet);
			Save(est);
		    }else{
			est = new Estudiante(estudiante.getId(), nombre,apellido,grupo,carnet);
			Update(est);
		    }
		    clearTexts();
		} catch (IOException ex) {
		    Logger.getLogger(DlgEstudiantesController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private void Save(Estudiante e) throws IOException{
		EstudianteModel.getDaoImpl().save(e);
		observableListEmpleados.add(e);
	}
	private void Update(Estudiante e) throws IOException{
		EstudianteModel.getDaoImpl().update(e);
		int index = observableListEmpleados.indexOf(estudiante);
		observableListEmpleados.set(index, e);
	}
	@FXML
	private void OnActionCancelar(ActionEvent event) {
		Node source = (Node) event.getSource();
		Stage stage = (Stage) source.getScene().getWindow();        
		stage.close();  
	}
	
	public void setObservableListEmpleados(ObservableList<Estudiante> observableListEmpleados) {
		this.observableListEmpleados = observableListEmpleados;
	}

	public void setEmpleado(Estudiante estudiante) {
	    this.estudiante = estudiante;
	    System.out.println(estudiante.toString());
	    txtNombre.setText(estudiante.getNombre());
	    txtApellido.setText(estudiante.getApellido());
	    txtGrupo.setText(estudiante.getGrupo());
	    txtCarnet.setText(estudiante.getCarnet());
	    btnGuardar.setText("Actualizar");
	}

	private void clearTexts() {
	    txtNombre.setText("");
	    txtApellido.setText("");
	    txtGrupo.setText("");
	    txtCarnet.setText("");
	}

}//Final



















































































































