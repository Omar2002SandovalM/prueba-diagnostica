/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.Sistemas.Controller;

import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ni.edu.uni.Sistemas.Entities.Estudiante;
import ni.edu.uni.Sistemas.Model.EstudianteModel;

/**
 * FXML Controller class
 *
 * @author Lenovo
 */
public class EstudiantesController implements Initializable {

	@FXML
	private VBox content_area;
	@FXML
	private TableView<Estudiante> tblEstudiantes;
	@FXML
	private TableColumn<Estudiante, String> Column_Id;
	@FXML
	private TableColumn<Estudiante, String> Column_Nombre;
	@FXML
	private TableColumn<Estudiante, String> Column_Apellido;
	@FXML
	private TableColumn<Estudiante, String> Column_Grupo;
	@FXML
	private TableColumn<Estudiante, String> Column_Carnet;
	@FXML
	private JFXButton btnAdd;
	@FXML
	private JFXButton btnEdit;
	@FXML
	private JFXButton bntDelete;

	/**
	 * Initializes the controller class.
	 * @param url
	 * @param rb
	 */
	private ObservableList<Estudiante> observableListEmpleado;

	private StringBuilder filter;
	@FXML
	private TextField txtFinder;

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// TODO
	    try {
		filter = new StringBuilder();
		observableListEmpleado = FXCollections.observableArrayList();
		Column_Nombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
		Column_Apellido.setCellValueFactory(new PropertyValueFactory<>("apellido"));
		Column_Grupo.setCellValueFactory(new PropertyValueFactory<>("grupo"));
		Column_Carnet.setCellValueFactory(new PropertyValueFactory<>("Carnet"));

		observableListEmpleado.addAll(EstudianteModel.getDaoImpl().findAll());
		tblEstudiantes.setItems(observableListEmpleado);

		FilteredList<Estudiante> filteredList = new FilteredList<>(observableListEmpleado, p -> true);				
		txtFinder.textProperty().addListener((observable, oldValue, newValue) -> {
				filteredList.setPredicate(b -> {
					if (newValue == null || newValue.isEmpty()) {
						return true;
					}
					String lowerCaseFilter = newValue.toLowerCase();
					
					if(b.getNombre().toLowerCase().contains(lowerCaseFilter)){
						return true;
					}
					if(b.getApellido().toLowerCase().contains(lowerCaseFilter))
					{
						return true;
					}
					if(b.getGrupo().toLowerCase().contains(lowerCaseFilter))
					{
						return true;
					}
					if(b.getCarnet().toLowerCase().contains(lowerCaseFilter))
					{
						return true;
					}
					return false;
				});
			});
			tblEstudiantes.setItems(filteredList);
	    } catch (IOException ex) {
		Logger.getLogger(EstudiantesController.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}	

	@FXML
	private void OnActionAdd(ActionEvent event) {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ni/edu/uni/Sistemas/Views/FXMLDlgEstudiante.fxml"));
			Parent parent = fxmlLoader.load();
			DlgEstudiantesController dialogController = fxmlLoader.getController();
			dialogController.setObservableListEmpleados(observableListEmpleado);

			Scene scene = new Scene(parent);
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.showAndWait();
		} catch (IOException ex) {
			Logger.getLogger(EstudiantesController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@FXML
	private void OnActionEdit(ActionEvent event) {
		 Estudiante selectedEst = (Estudiante) tblEstudiantes.getSelectionModel().getSelectedItem();
		try {
		    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ni/edu/uni/Sistemas/Views/FXMLDlgEstudiante.fxml"));
		    Parent parent = fxmlLoader.load();
		    DlgEstudiantesController dialogController = fxmlLoader.getController();
		    dialogController.setObservableListEmpleados(observableListEmpleado);
		    dialogController.setEmpleado(selectedEst);

		    Scene scene = new Scene(parent);
		    Stage stage = new Stage();
		    stage.initModality(Modality.APPLICATION_MODAL);
		    stage.setScene(scene);
		    stage.showAndWait();
		    System.gc();
		} catch (IOException ex) {
		    Logger.getLogger(EstudiantesController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@FXML
	private void OnActionDelete(ActionEvent event) {
		try {
			System.gc();
			Estudiante selectedEst = (Estudiante) tblEstudiantes.getSelectionModel().getSelectedItem();
			EstudianteModel.getDaoImpl().delete(selectedEst);
			observableListEmpleado.remove(selectedEst);            
		} catch (IOException ex) {
			Logger.getLogger(EstudiantesController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
}














































































