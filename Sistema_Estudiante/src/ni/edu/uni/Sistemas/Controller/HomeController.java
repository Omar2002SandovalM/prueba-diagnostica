/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.Sistemas.Controller;

import com.jfoenix.controls.JFXComboBox;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javax.swing.JOptionPane;
import ni.edu.uni.Sistemas.Entities.Asignatura;
import ni.edu.uni.Sistemas.Entities.Estudiante;
import ni.edu.uni.Sistemas.Entities.Inscripcion;
import ni.edu.uni.Sistemas.Model.AsignaturaModel;
import ni.edu.uni.Sistemas.Model.EstudianteModel;
import ni.edu.uni.Sistemas.Model.InscripcionModel;

/**
 * FXML Controller class
 *
 * @author Lenovo
 */
public class HomeController implements Initializable {

	@FXML
	private VBox content_area;
	@FXML
	private TableView<Inscripcion> tblRegistros;
	@FXML
	private TableColumn<Inscripcion, String> ColumnGrupo;
	@FXML
	private TableColumn<Inscripcion, String> ColumnEstudiante;
	@FXML
	private TableColumn<Inscripcion, String> ColumnAsignatura;
	@FXML
	private JFXComboBox<String> cmbAsignatura;
	@FXML
	private JFXComboBox<String> cmbEstudiante;
	@FXML
	private JFXComboBox<String> cmbGrupo;
	@FXML
	private Button btnNuevo;
	@FXML
	private Button btnEliminar;

	/**
	 * Initializes the controller class.
	 */
	private ObservableList<Inscripcion> observableListInscripcions;
	private ObservableList<Estudiante> observableListEstudiantes;
	private ObservableList<Asignatura> observableListAsignaturas;

	@FXML
	private TextField txtFinder;
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		try {
			// TODO
			observableListEstudiantes = FXCollections.observableArrayList();
			observableListAsignaturas = FXCollections.observableArrayList();
			
			observableListEstudiantes.setAll(EstudianteModel.getDaoImpl().findAll());
			ObservableList<String> lista1 = FXCollections.observableArrayList();
			ObservableList<String> lista2 = FXCollections.observableArrayList();
			for (Estudiante e : observableListEstudiantes) {
				lista1.add(e.getNombre());
				lista2.add(e.getGrupo());
			}
			cmbEstudiante.setItems(lista1);
			cmbGrupo.setItems(lista2);
			
			observableListAsignaturas.setAll(AsignaturaModel.getDaoImpl().findAll());
			ObservableList<String> lista3 = FXCollections.observableArrayList();
			for (Asignatura e : observableListAsignaturas) {
				lista3.add(e.getNombre());
			}
			cmbAsignatura.setItems(lista3);
			
			observableListInscripcions = FXCollections.observableArrayList();
			observableListInscripcions.setAll(InscripcionModel.getDaoImpl().findAll());
			tblRegistros.setItems(observableListInscripcions);
			
			ColumnGrupo.setCellValueFactory( new PropertyValueFactory<>("grupo"));
			ColumnEstudiante.setCellValueFactory( new PropertyValueFactory<>("Estudiante"));
			ColumnAsignatura.setCellValueFactory( new PropertyValueFactory<>("Asignatura"));
			
			
			FilteredList<Inscripcion> filteredList = new FilteredList<>(observableListInscripcions, p -> true);
			txtFinder.textProperty().addListener((observable, oldValue, newValue) -> {
				filteredList.setPredicate(b -> {
					if (newValue == null || newValue.isEmpty()) {
						return true;
					}
					String lowerCaseFilter = newValue.toLowerCase();	
					if(b.getGrupo().toLowerCase().contains(lowerCaseFilter)){
						return true;
					}
					if(b.getEstudiante().toLowerCase().contains(lowerCaseFilter)){
						return true;
					}
					if(b.getAsignatura().toLowerCase().contains(lowerCaseFilter)){
						return true;
					}
					return false;
				});
			});
			
			tblRegistros.setItems(filteredList);
		} catch (IOException ex) {
			Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}	

	@FXML
	private void OnActionNuevoi(ActionEvent event) {
		String Grupo,estu, asig;
		
		Grupo = cmbGrupo.getValue();
		estu = cmbEstudiante.getValue();
		asig = cmbAsignatura.getValue();
		
		Inscripcion as = null;
		try {
			as = new Inscripcion(estu, asig, Grupo);
			save(as);
		} catch (IOException ex) {
		    Logger.getLogger(DlgAsignaturaController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@FXML
	private void OnActionDelete(ActionEvent event) throws IOException {
		System.gc();
		Inscripcion selected = tblRegistros.getSelectionModel().getSelectedItem();
		if(selected == null)
		{
			JOptionPane.showMessageDialog(null, "Debe seleccionar una Fila para Eliminar");
			return;
		}
		InscripcionModel.getDaoImpl().delete(selected);
		observableListInscripcions.remove(selected);
	}

	private void save(Inscripcion as) throws IOException {
		InscripcionModel.getDaoImpl().save(as);
		observableListInscripcions.add(as);
	}
	
}






















































































































