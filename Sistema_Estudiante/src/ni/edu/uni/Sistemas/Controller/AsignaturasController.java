/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.Sistemas.Controller;

import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ni.edu.uni.Sistemas.Entities.Asignatura;
import ni.edu.uni.Sistemas.Model.AsignaturaModel;

/**
 * FXML Controller class
 *
 * @author Lenovo
 */
public class AsignaturasController implements Initializable {

	@FXML
	private VBox content_area;
	@FXML
	private JFXButton btnAdd;
	@FXML
	private JFXButton btnEdit;
	@FXML
	private JFXButton bntDelete;
	@FXML
	private TableView<Asignatura> tblAsignaturas;
	@FXML
	private TableColumn<Asignatura, String> Column_Id;
	@FXML
	private TableColumn<Asignatura, String> Column_Asignatura;

	/**
	 * Initializes the controller class.
	 * @param url
	 * @param rb
	 */
	private ObservableList<Asignatura> observableListEmpleado;

	private StringBuilder filter;
	@FXML
	private TextField txtFinder;
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// TODO
		try {
			filter = new StringBuilder();
			observableListEmpleado = FXCollections.observableArrayList();
			Column_Asignatura.setCellValueFactory(new PropertyValueFactory<>("Nombre"));

			observableListEmpleado.addAll(AsignaturaModel.getDaoImpl().findAll());
			tblAsignaturas.setItems(observableListEmpleado);

			FilteredList<Asignatura> filteredList = new FilteredList<>(observableListEmpleado, p -> true);				
			txtFinder.textProperty().addListener((observable, oldValue, newValue) -> {
				filteredList.setPredicate(b -> {
					if (newValue == null || newValue.isEmpty()) {
						return true;
					}
					String lowerCaseFilter = newValue.toLowerCase();
					
					if(b.getNombre().toLowerCase().contains(lowerCaseFilter)){
						return true;
					}
					return false;
				});
			});
			tblAsignaturas.setItems(filteredList);
		
	    } catch (IOException ex) {
		Logger.getLogger(EstudiantesController.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}	

	@FXML
	private void OnActionAdd(ActionEvent event) {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ni/edu/uni/Sistemas/Views/FXMLDlgAsignatura.fxml"));
			Parent parent = fxmlLoader.load();
			DlgAsignaturaController dialogController = fxmlLoader.getController();
			dialogController.setObservableListEmpleados(observableListEmpleado);

			Scene scene = new Scene(parent);
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.showAndWait();
		} catch (IOException ex) {
			Logger.getLogger(AsignaturasController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@FXML
	private void OnActionEdit(ActionEvent event) {
		Asignatura selectedAsig = (Asignatura) tblAsignaturas.getSelectionModel().getSelectedItem();
		try {
		    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ni/edu/uni/Sistemas/Views/FXMLDlgAsignatura.fxml"));
		    Parent parent = fxmlLoader.load();
		    DlgAsignaturaController dialogController = fxmlLoader.getController();
		    dialogController.setObservableListEmpleados(observableListEmpleado);
		    dialogController.setEmpleado(selectedAsig);

		    Scene scene = new Scene(parent);
		    Stage stage = new Stage();
		    stage.initModality(Modality.APPLICATION_MODAL);
		    stage.setScene(scene);
		    stage.showAndWait();
		    System.gc();
		} catch (IOException ex) {
		    Logger.getLogger(AsignaturasController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@FXML
	private void OnActionDelete(ActionEvent event) {
		
		try {
			System.gc();
			Asignatura selectedAsig = (Asignatura) tblAsignaturas.getSelectionModel().getSelectedItem();
			AsignaturaModel.getDaoImpl().delete(selectedAsig);
			observableListEmpleado.remove(selectedAsig);            
		} catch (IOException ex) {
			Logger.getLogger(EstudiantesController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
}






















































