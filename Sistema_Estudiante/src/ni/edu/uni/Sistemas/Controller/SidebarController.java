/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.Sistemas.Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Lenovo
 */
public class SidebarController implements Initializable {

	@FXML
	private VBox VBox_SidebarContent;
	@FXML
	private Button btn_Home;
	@FXML
	private Button btn_Estudiantes;
	@FXML
	private Button btn_Asignaturas;

	/**
	 * Initializes the controller class.
	 * @param url
	 * @param rb
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// TODO
	}	

	@FXML
	private void OnActionBtnHome(ActionEvent event) throws IOException {
		boolean flag = true;
		BorderPane border_pane  = (BorderPane) ((Node) event.getSource()).getScene().getRoot();
		
		if(flag == true){
			FXMLLoader loader = new  FXMLLoader();
			
			Parent node = loader.load(getClass().getResource("/ni/edu/uni/Sistemas/Views/FXMLHome.fxml"));
			
			border_pane.setCenter(node);
			
			flag = false;
		} else if(flag == false){
			System.out.println("Ya tiene Abierta Una Pestana");
		}
	}

	@FXML
	private void OnActionEstudinates(ActionEvent event) throws IOException {
		boolean flag = true;
		BorderPane border_pane  = (BorderPane) ((Node) event.getSource()).getScene().getRoot();
		
		if(flag == true){
			FXMLLoader loader = new  FXMLLoader();
			
			Parent node = loader.load(getClass().getResource("/ni/edu/uni/Sistemas/Views/FXMLEstudiante.fxml"));
			
			border_pane.setCenter(node);
			
			flag = false;
		} else if(flag == false){
			System.out.println("Ya tiene Abierta Una Pestana");
		}
	}

	@FXML
	private void OnActionAsignaturas(ActionEvent event) throws IOException {
		boolean flag = true;
		BorderPane border_pane  = (BorderPane) ((Node) event.getSource()).getScene().getRoot();
		
		if(flag == true){
			FXMLLoader loader = new  FXMLLoader();
			
			Parent node = loader.load(getClass().getResource("/ni/edu/uni/Sistemas/Views/FXMLAsignatura.fxml"));
			
			border_pane.setCenter(node);
			
			flag = false;
		} else if(flag == false){
			System.out.println("Ya tiene Abierta Una Pestana");
		}
	}
	
}



















