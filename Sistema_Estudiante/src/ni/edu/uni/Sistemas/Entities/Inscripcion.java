/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.Sistemas.Entities;

/**
 *
 * @author Lenovo
 */
public class Inscripcion {
	
	private int id;
	private String Estudiante;
	private String Asignatura;
	private String grupo;

	public Inscripcion() {
	}

	public Inscripcion(int id, String Estudiante, String Asignatura, String grup) {
		this.id = id;
		this.Estudiante = Estudiante;
		this.Asignatura = Asignatura;
		this.grupo = grup;
	}

	public Inscripcion(String Estudiante, String Asignatura, String gru) {
		this.Estudiante = Estudiante;
		this.Asignatura = Asignatura;
		this.grupo = gru;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEstudiante() {
		return Estudiante;
	}

	public void setEstudiante(String Estudiante) {
		this.Estudiante = Estudiante;
	}

	public String getAsignatura() {
		return Asignatura;
	}

	public void setAsignatura(String Asignatura) {
		this.Asignatura = Asignatura;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	
	
}//Final de la clase












