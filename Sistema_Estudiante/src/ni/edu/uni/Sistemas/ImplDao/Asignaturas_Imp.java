/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.Sistemas.ImplDao;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ni.edu.uni.Sistemas.Dao.IDao_Asignatura;
import ni.edu.uni.Sistemas.Entities.Asignatura;
import ni.edu.uni.Sistemas.Util.RandomFileBinarySearch;

/**
 *
 * @author Lenovo
 */
public class Asignaturas_Imp implements IDao_Asignatura{
	
	private RandomAccessFile hraf;
	private RandomAccessFile draf;
	private final int SIZE = 154;
	private final String HEADER_FILE_NAME = "hAsignaturas.dat";
	private final String DATA_FILE_NAME = "dAsignaturas.dat";

	public Asignaturas_Imp() {
	    RandomFileBinarySearch.SIZE = 4;
	}

	private void open() throws IOException {

	    File hfile = new File(HEADER_FILE_NAME);
	    File dfile = new File(DATA_FILE_NAME);

	    if (!hfile.exists()) {
		hfile.createNewFile();
		hraf = new RandomAccessFile(hfile, "rw");
		draf = new RandomAccessFile(dfile, "rw");
		hraf.seek(0);
		hraf.writeInt(0);
		hraf.writeInt(0);
	    } else {
		hraf = new RandomAccessFile(hfile, "rw");
		draf = new RandomAccessFile(dfile, "rw");
	    }

	}

	private void close() throws IOException {
	    if (hraf != null) {
		hraf.close();
		hraf = null;
	    }

	    if (draf != null) {
		draf.close();
		draf = null;
	    }
	}
	
	@Override
	public Asignatura buscarPorId(int id) throws IOException {
		open();
		hraf.seek(0);
		int n = hraf.readInt();
		int k = hraf.readInt();
		if(n == 0){
		    close();
		    return null;
		}

		RandomFileBinarySearch.hraf = hraf;
		int pos = RandomFileBinarySearch.runBinarySearchRecursively(id, 0, n);
		if (pos < 0) {
		    close();
		    return null;
		}
		long hpos = 8 + 4 * (pos);
		hraf.seek(hpos);
		int index = hraf.readInt();

		long dpos = (index - 1) * SIZE;
		draf.seek(dpos);

		Asignatura a = new Asignatura();
		a.setId(draf.readInt());
		a.setNombre(draf.readUTF());
		close();

		return a;

	}

	@Override
	public void save(Asignatura t) throws IOException {
		open();
		hraf.seek(0);
		int n = hraf.readInt();
		int k = hraf.readInt();

		long pos = k * SIZE;

		draf.seek(pos);

		draf.writeInt(++k);//ID
		draf.writeUTF(t.getNombre());
		
		hraf.seek(0);
		hraf.writeInt(++n);
		hraf.writeInt(k);

		long hpos = 8 + 4 * (n - 1);
		hraf.seek(hpos);
		hraf.writeInt(k);
		close();
	}

	@Override
	public int update(Asignatura t) throws IOException {
		open();
		hraf.seek(0);
		RandomFileBinarySearch.hraf = hraf;
		int n = hraf.readInt();
		int pos = RandomFileBinarySearch.runBinarySearchRecursively(t.getId(), 0, n);
		if (pos < 0) {
		    close();
		    return pos;
		}
		long hpos = 8 + 4 * (pos);
		hraf.seek(hpos);
		int id = hraf.readInt();

		long dpos = (id - 1) * SIZE;
		draf.seek(dpos);

		draf.writeInt(t.getId());
		draf.writeUTF(t.getNombre());
		
		close();
		return t.getId();
	}

	@Override
	public boolean delete(Asignatura t) throws IOException {
		  File tmp = new File("tmp_Asignatura.dat");
		try (RandomAccessFile tmpraf = new RandomAccessFile(tmp, "rw")) {
		    open();
		    hraf.seek(0);
		    int n = hraf.readInt();
		    int k = hraf.readInt();

		    tmpraf.seek(0);
		    tmpraf.writeInt(n - 1);
		    tmpraf.writeInt(k);
		    int j = 0;
		    for (int i = 0; i < n; i++) {
			long hpos = 8 + 4 * i;
			hraf.seek(hpos);
			int id = hraf.readInt();
			if (id == t.getId()) {
			    continue;
			}
			long tmpos = 8 + 4 * j++;
			tmpraf.seek(tmpos);
			tmpraf.writeInt(id);
		    }
		    close();
		}
		close();
		File f = new File(HEADER_FILE_NAME);
		boolean flag = f.delete();
		if (flag) {
		    tmp.renameTo(f);
		} else {
		    Logger.getLogger(Asignaturas_Imp.class.getName()).log(Level.SEVERE, "ERROR, no se pudo eliminar el archivo!");
		}
		return flag;
	}

	@Override
	public List<Asignatura> findAll() throws IOException {
		open();
		List<Asignatura> all = new ArrayList<>();
		hraf.seek(0);
		int n = hraf.readInt();

		for (int i = 0; i < n; i++) {

			long hpos = 8 + 4 * i;
			hraf.seek(hpos);
			int id = hraf.readInt();

			long dpos = (id - 1) * SIZE;
			draf.seek(dpos);

		    Asignatura m = new Asignatura();
		    m.setId(draf.readInt());//Id
		    m.setNombre(draf.readUTF());//Nombre
		    all.add(m);
		}

		close();
		return all;
	}
}









































































